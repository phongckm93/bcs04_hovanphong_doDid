import { Work } from "./model.js";
export let idSearch = 0;
export let info = () => {
    let toDo = document.getElementById("newTask").value;
    if (toDo == "") {
        return -1;
    }
    return new Work(toDo);
};
export let renderWork = (list) => {
    let contentHTML = "";
    list.forEach((work) => {
        let content = ` <section>
        <span>${work.id}. ${work.description}</span>
        <div>
        <button onclick="completeWork('${work.id}')"><i class="fas fa-calendar-check"></i></button>
        <button onclick="editWork('${work.id}')"><a href="#top"><i class="fas fa-pen-fancy"></i></a></button>
        <button onclick="deleteWork('${work.id}')"><i class="fas fa-times"></i></button>
        </div>
        </section>
        `;
        contentHTML += content;
    });
    document.getElementById("todo").innerHTML = contentHTML;
};
export let edit = (list) => {
    document.getElementById("newTask").value = list.description;
    idSearch = list.id;
};
export let resetInput = () => {
    document.getElementById("newTask").value = "";
    document.getElementById("addItem").style.display = "block";
    document.getElementById("upItem").style.display = "none";
    document.getElementById("hidden").style.display = "none";

};
export let listCom = [];
let DID_LOCAL = "DID_LOCAL";
let didJson = localStorage.getItem(DID_LOCAL);
if (didJson != null) {
    listCom = JSON.parse(didJson);
}
export let renderDid = (list) => {
    let didWork = "";
    list.forEach(work => {
        let content = ` <section>
        <p>${work.id}. ${work.description}</p>
        <div>
        <button><i class="fas fa-calendar-check"></i></button>
        <button onclick="deleteDid('${work.id}')"><i class="fas fa-times"></i></button>
        </div>
        </section>
        `;
        didWork += content;
    });
    document.getElementById("completed").innerHTML = didWork;
    didJson = JSON.stringify(listCom);
    localStorage.setItem(DID_LOCAL, didJson);
};
export let onOffLoading = () => {
    let change = document.getElementById("onOff");
    if (change.style.display == "none") {
        change.style.display = "block"
    } else { change.style.display = "none" }
}
