import {
    renderWork,
    info,
    edit,
    idSearch,
    resetInput,
    renderDid,
    listCom,
    onOffLoading,
} from "./control.js";
const BASE_URL = "https://62db6ca4d1d97b9e0c4f338f.mockapi.io";
document.getElementById("date").innerHTML = new Date();
getWork();
renderDid(listCom)
function getWork() {
    axios({
        url: `${BASE_URL}/work`,
        method: "GET",
    })
        .then(function (res) {
            onOffLoading();
            renderWork(res.data);
        })
        .catch(function (err) {
            alert("kiểm tra kêt nối mạng")
            console.log(err);
        });
}
function deleteWork(id) {
    axios({
        url: `${BASE_URL}/work/${id}`,
        method: "DELETE",
    })
        .then(function (res) {
            console.log("delete", res);
            getWork();
            onOffLoading();
        })
        .catch(function (err) {
            alert("kiểm tra kêt nối mạng")
            console.log(err);
        });
    resetInput();
}
window.deleteWork = deleteWork;
document.getElementById("addItem").addEventListener("click", () => {
    let addWork = info();
    if (addWork == -1) {
        alert("Xin hãy nhập dữ liệu");
        return;
    }
    axios({
        url: `${BASE_URL}/work`,
        method: "POST",
        data: addWork,
    })
        .then(function (res) {
            console.log("addwork", res);
            getWork();
            onOffLoading();
        })
        .catch(function (err) {
            onOffLoading();
            alert("kiểm tra kêt nối mạng")
            console.log(err);
        });
    resetInput();
});

function editWork(id) {
    document.getElementById("addItem").style.display = "none";
    document.getElementById("upItem").style.display = "block";
    document.getElementById("hidden").style.display = "block"

    axios({
        url: `${BASE_URL}/work/${id}`,
        method: "GET",
    })
        .then(function (res) {
            console.log("edit", res);
            edit(res.data);
        })
        .catch(function (err) {
            alert("kiểm tra kêt nối mạng")
            console.log(err);
        });
}
window.editWork = editWork;
document.getElementById("upItem").addEventListener("click", () => {
    let a = info();
    axios({
        url: `${BASE_URL}/work/${idSearch}`,
        method: "PUT",
        data: a,
    })
        .then(function (res) {
            console.log("updateWork", res);
            getWork();
            onOffLoading();
        })
        .catch(function (err) {
            alert("kiểm tra kêt nối mạng")
            onOffLoading();
            console.log(err);
        });
    resetInput();
});
function completeWork(id) {
    axios({
        url: `${BASE_URL}/work/${id}`,
        method: "DELETE",
    })
        .then(function (res) {
            getWork();
            console.log("comWork", res);
            listCom.push(res.data)
            renderDid(listCom);
            onOffLoading();
        })
        .catch(function (err) {
            alert("kiểm tra kêt nối mạng");
            onOffLoading();
            console.log(err);
        });
}
window.completeWork = completeWork;
function deleteDid(number) {
    let index = listCom.findIndex((item) => { return item.id == number })
    listCom.splice(index, 1)
    renderDid(listCom);
}
window.deleteDid = deleteDid;

